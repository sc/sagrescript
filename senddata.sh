#!/bin/bash
moist=17

echo $moist >  /sys/class/gpio/export
sleep 1
echo in > /sys/class/gpio/gpio$moist/direction
sleep 1

while ! ping -c1 192.168.1.19 &>/dev/null; do echo "Ping Fail - `date`"; done ; 
echo "Sagregang found- `date`" 

while true; do
	water=`cat /sys/class/gpio/gpio$moist/value`

	outsite1=$(cat /sys/devices/w1_bus_master1/28-00000a17a157/w1_slave | grep t= | sed "s/t=//g" | awk '{print $10}')
	outsite2=$(cat /sys/devices/w1_bus_master1/28-00000a4c8a73/w1_slave | grep t= | sed "s/t=//g" | awk '{print $10}')
	tdate=$(date -d "today" +"%Y%m%d%H%M")
	echo "$tdate","$water","$outsite1","$outsite2" | ssh -i /home/pi/.ssh/id_rsa frondmin@192.168.1.19 "cat >> ceresdata.csv"
	sleep 600
done

