#!/bin/bash
echo "ceres send start signal - `date`">>/home/pi/logceres; 
/bin/bash /home/pi/sagrescript/startstopceres.sh 1
sleep 240
while ping -c1 192.168.1.101 &>/dev/null; do 
	echo "ceres connected - `date`">>/home/pi/logceres; 
	sleep 10 ; 
done ; 
echo "Ceres Disconnected (send stop signal) - `date`">>/home/pi/logceres ; 
bash /home/pi/sagrescript/startstopceres.sh 0


