onoff=$1
ceres=27 
if [ ! -d "/sys/class/gpio/gpio$ceres" ]; then
  echo $ceres > /sys/class/gpio/export || { echo -e "Can't access GPIO $ceres" 1>&2; exit 1; }
  sleep 1
fi

echo out > /sys/class/gpio/gpio$ceres/direction || { echo -e "Can't set GPIO $ceres to output" 1>&2; exit 1; }

echo "$onoff" > /sys/class/gpio/gpio$ceres/value
