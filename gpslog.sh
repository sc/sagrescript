#!/bin/bash
out="/home/pi/gps"
tty="/dev/ttyUSB0"
isset=0
echo "log of $tty start in $out"

while true ;
do

        if [ -f $tty ]
        then
                if [ "$isset" -eq 0 ]
                then
                        stty -F $tty 9600
                        isset=1
                fi
                cat $tty >> $out ;
        else
                isset=0
                #echo "no device found"
                sleep 1
        fi
done


