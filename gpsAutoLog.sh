#!/bin/bash
out="/home/pi/gps"
tty="/dev/ttyUSB0"
isset=0
echo "log of start in $out"

while true ; 
do 

	if [ -e $tty ] 
	then
		if [ $isset -eq 0 ]
		then
			stty -F $tty raw 9600 cs8 clocal -cstopb
			isset=1
		fi
		cat $tty >> $out ; 
	else
		isset=0
		sleep 1
	fi
done


