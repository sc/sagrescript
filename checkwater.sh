#!/bin/bash
echo "start checkwater"

moist=17
relay=2

echo $relay >  /sys/class/gpio/export
sleep 1
echo $moist >  /sys/class/gpio/export
sleep 1
echo out > /sys/class/gpio/gpio$relay/direction
sleep 1
echo in > /sys/class/gpio/gpio$moist/direction

echo "let's wait for any time update"
sleep 240

echo "Now we can set the poweroff in 2hours"

sudo shutdown -P +120

tdate=$(date -d "today" +"%Y%m%d%H%M")
echo "$tdate start monitoring moist" | ssh -i /home/pi/.ssh/id_rsa frondmin@192.168.1.19 "cat >> cereslog"
while true;
do
	tdate=$(date -d "today" +"%Y%m%d%H%M")
	water=`cat /sys/class/gpio/gpio$moist/value`
	if  [ $water -ge 1 ] ;
	then
		echo "$tdate watering closed" | ssh -i /home/pi/.ssh/id_rsa frondmin@192.168.1.19 "cat >> cereslog"
		echo 0 > /sys/class/gpio/gpio$relay/value
	else
		echo "$tdate watering open" | ssh -i /home/pi/.ssh/id_rsa frondmin@192.168.1.19 "cat >> cereslog"
		echo 1 > /sys/class/gpio/gpio$relay/value
	fi
	sleep 60
done


