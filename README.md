
#General For the PI

From:https://www.raspberrypi.org/documentation/installation/installing-images/linux.md

```bash
dd bs=4M if=2018-04-18-raspbian-stretch-lite.img of=/dev/mmcblk0 conv=fsync status=progress
##I remember that I ha d problem of image badly copied so the test of the image was a good ide
```

To connect to the pi 0 through TTL

* rangee de droite quand SD est en haut: pin 1= 5V, pin 2 rien , pin 3:5 black(GND),white(TX),green(RX)
* then : `screen /dev/ttyUSB0 115200` (probablement en sudo)

## SSH and INternet

```bash
echo "country=FR
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
    ssid="cacaprout"
    psk="nintendo"
    key_mgmt=WPA-PSK
}" > /media/$USER/boot/wpa_supplicant.conf
```

```bash
touch /media/$USER/boot/ssh
```

# GPS using TTL to USB
* plug the ttl on the GPS: red, black,green,white
* use stty -F /dev/ttyUSB0 9600 to se the ttl at the right speed
then cat /dev/ttyUSB0 >> output
 A priori je pourrais juste utiliser gpsd, qui fait pareil mais en traitant correctement les donnee

Le donnée GPS son au format NMEA. Et c'est pa top. Masia vec cette commande on peut faire du gpx:
```bash
gpsbabel -i nmea -f gps.8 -x discard,hdop=10 -o gpx -F "outputfilename.gpx"
```

#### not about thi pin:
* red = POWER (5V) ; green == RX, black == GROUND, white == TX 

Script on raspberry:
* mv statbat.sh /etc/init.d/statbat
* chmod 755 /etc/init.d/statbat


# Pour foutre internet via PPP
source:http://www.instructables.com/id/Connect-the-Raspberry-Pi-to-network-using-UART/

Install PPP on the pi and the computer connected to internet.

To install the pi from .deb :
* http://archive.raspbian.org/raspbian/pool/main/libp/libpcap/
* http://archive.raspbian.org/raspbian/pool/main/p/ppp/


then dpkg -i libpcap and ppp on the pi

on the pi:
stty -F /dev/ttyAMA0 raw
pppd /dev/ttyAMA0 115200 192.168.1.37:192.168.1.36 noauth local debug dump defaultroute nocrtscts
pppd /dev/ttyAMA0 115200 192.168.1.37:192.168.1.36 debug nodetach noauth silent local nocrtscts xonxoff maxfail 0 proxyarp
(et mettre ,a dans un putain de scrip init.d)


on the computer:
pppd /dev/ttyUSB0 115200 192.168.1.102:192.168.1.101 proxyarp local noauth debug nodetach dump nocrtscts passive persist maxfail 0 holdoff 1


# Random not:
problem with ttl:

open file:
`/media/scarrign/boot/cmdline.txt`

and set console=ttyAMA0

# Boot start script:

CF [this file](https://framagit.org/sc/sagrescript/blob/master/gpsd) ou ausis [la](https://www.stuffaboutcode.com/2012/06/raspberry-pi-run-program-at-start-up.html)

faire un fichier `/etc/init.d/daemond` qui lancera le script (qui pourra etre mis dans `/usr/bin/script.sh`
installer les script 

```bash
chmod a+x $HOME/sagrescript/alltemp.sh 
ln -s $HOME//sagrescript/alltemp.sh /usr/bin/alltemp
chmod a+x $HOME/sagrescript/takepics.sh 
ln -s $HOME/sagrescript/takepics.sh /usr/bin/takepics
```

```bash
chmod a+x $HOME/sagrescript/alltempd 
ln -s $HOME//sagrescript/alltempd /etc/init.d/alltempd
chmod a+x $HOME/sagrescript/takepicsd 
ln -s $HOME//sagrescript/takepicsd /etc/init.d/takepicsd
```

###USE GPIO
```bash
echo 17 > /sys/class/gpio/export  
echo out > /sys/class/gpio/gpio17/direction 
echo 1 > /sys/class/gpio/gpio17/value
```

# CERES : 
Serial should be : blanc jaune vert u

while true; do cat /sys/class/gpio/gpio3/value  >> logwet ; cat /sys/devices/w1_bus_master1/28-00000a4c8a73/w1_slave| grep "t="  >> logwet ; cat /sys/devices/w1_bus_master1/28-00000a17a157/w1_slave| grep "t="  >> logwet ; date >> logwet ; sleep 1 ; done


Be carefull that this need a new version of libc6, should download an update version of raspbian

wget http://archive.raspbian.org/raspbian/pool/main/libp/libpcap/libpcap0.8_1.8.1-6_armhf.deb
wget  http://archive.raspbian.org/raspbian/pool/main/p/ppp/ppp_2.4.7-2%2B4.1_armhf.deb
wget http://archive.raspbian.org/raspbian/pool/main/g/glibc/libc6_2.28-8_armhf.deb

to install the send data service: bash install sendata c'est du lourd et je peux faire pareil avec installPPPD

!!ATTENTION AUSSI IL FAUT AU MOINS SE CONNECTER UNE FOIS EN ROOT POUR ACCEPTER LE CERTIFICAAAAAT

To setup the raspberry with pppd i found some info here :
https://www.instructables.com/id/Connect-the-Raspberry-Pi-to-network-using-UART/

but stil the more easy is to no care with the service the way he does and do our, you can install it thought bash installPPD and that's it. Then on the raspby pi host (chepapi), you should un:
pppd /dev/ttyUSB0 115200 192.168.1.102:192.168.1.101 proxyarp local noauth debug nodetach dump nocrtscts passive persist maxfail 0 holdoff 1


and also allow the network to be forwarded.

### DHT11
`https://wiki.52pi.com/images/f/f6/13.How_to_use_DHT11_temperature_sensor_to_detect_the_temperature_in_the_room.pdf`
gcc -Wall -pthread -o DHTXXD test_DHTXXD.c DHTXXD.c -lpigpiod_if2

sudo pigpiod
./DHTXXD -g4 # assuming you are using Broadcom GPIO 4

http://abyz.me.uk/rpi/pigpio/examples.html#pdif2_DHTXXD
