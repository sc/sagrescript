file=$1
bn=`basename $file`
unames=`echo $bn | sed "s/_/ /g" | sed "s/\.jpg//g"`
convert $file  -fill white  -pointsize 60 -gravity center -annotate +0+5 "$unames" tmp
mv tmp $file
